$( () => {

    // Fridget's Firebase Configuration
    let firebaseConfig = {
        apiKey: "AIzaSyB4YmE78xbn6NdtsQ4lFTAIveEp7JgXwHE",
        authDomain: "fridget-5afbc.firebaseapp.com",
        databaseURL: "https://fridget-5afbc.firebaseio.com",
        projectId: "fridget-5afbc",
        storageBucket: "fridget-5afbc.appspot.com",
        messagingSenderId: "754437151433",
        appId: "1:754437151433:web:52160b98eef12f99790b18",
        measurementId: "G-PWHM6RFSC7"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    
    let db = firebase.firestore();

    // Run basic query to retrieve Fridget items
    let items = [];
    db.collection("items").withConverter( itemConverter ).get().then( ( querySnapshot ) => {
        querySnapshot.forEach( ( doc ) => {
            items.push( doc.data() );
        });

        // Render items on the front-end
        let itemList = $( '.grocery-list' );
        items.forEach( ( item ) => {
            let itemElement = document.createElement( 'div' ),
                itemImage = document.createElement( 'div' ),
                itemName = document.createElement( 'p' ),
                itemChevron = document.createElement( 'img' );
            itemElement.className = 'grocery-item';
            itemImage.className = 'grocery-item__image';
            itemName.className = 'grocery-item__name';
            itemName.textContent = item.name;
            itemChevron.className = 'grocery-item__chevron';
            itemChevron.src = 'icons/chevron-right-solid.svg';
            itemElement.append( itemImage, itemName, itemChevron );
            itemList.append( itemElement );
        });
    });
});

class Item {
    constructor ( name ) {
        this.name = name;
    }
}

// Firestore Item Converter
let itemConverter = {
    toFirestore: function( item ) {
        return {
            name: item.name
        }
    },
    fromFirestore: function( snapshot, options ) {
        const data = snapshot.data( options );
        return new Item( data.name );
    }
}
